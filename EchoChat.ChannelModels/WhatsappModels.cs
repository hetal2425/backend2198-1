﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.ChannelModels
{
    public class WhatsAPPMessage
    {
        public string to { get; set; }
        public string type { get; set; }
        public Template template { get; set; }
    }

    public class Template
    {
        public string _namespace { get; set; }
        public string name { get; set; }
        public Language language { get; set; }
        public List<Component> components { get; set; }
    }

    public class Language
    {
        public string policy { get; set; }
        public string code { get; set; }
    }

    public class Component
    {
        public string type { get; set; }
        public string sub_type { get; set; }
        public string text { get; set; }
        public string index { get; set; }
        public List<Parameter> parameters { get; set; }

    }

    public class Parameter
    {
        public string type { get; set; }
        public string text { get; set; }
        public image image { get; set; }
        public video video { get; set; }
        public document document { get; set; }
    }

    public class image
    {
        public string link { get; set; }
    }

    public class video
    {
        public string link { get; set; }
    }
    public class document
    {
        public string link { get; set; }
        public string filename { get; set; }

        public document(string filename)
        {
            this.filename = filename;
        }
    }
    public class WAResponse
    {
        public List<Message> messages { get; set; }
        public Meta meta { get; set; }
    }

    public class Meta
    {
        public string api_status { get; set; }
        public string version { get; set; }
    }

    public class Message
    {
        public string id { get; set; }
    }

    //contact response

    public class Contact
    {
        public string input { get; set; }
        public string status { get; set; }
        public string wa_id { get; set; }
    }

    public class ContactResponse
    {
        public List<Contact> contacts { get; set; }
        public List<Error> errors { get; set; }
        public Meta meta { get; set; }
    }

    public class Error
    {
        public int code { get; set; }
        public string title { get; set; }
        public string details { get; set; }
    }


    public class WAUsers
    {
        public List<User> users { get; set; }
        public Meta meta { get; set; }
    }

    public class User
    {
        public string token { get; set; }
        public string expires_after { get; set; }
    }


    public class TemplateModel
    {
        public string Namespace { get; set; }
        public string Name { get; set; }
        public string Language { get; set; }
        public bool HeaderPresent { get; set; }
        public bool IsGeneric { get; set; }
        public bool IsWithoutParams { get; set; }
        public List<TemplateButtons> Buttons { get; set; }
    }

    public class TemplateButtons
    {
        public string ButtonType { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
        public string CountryCode { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsStatic { get; set; }
    }

}
