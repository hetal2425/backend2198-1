﻿using AutoMapper;
using EchoChat.DB;
using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EchoChat.Repository
{
    public class TemplateRepository : ITemplateRepository
    {
        IRepositoryBase _repositoryBase = null;
        private readonly IMapper _mapper;
        public TemplateRepository(IRepositoryBase repositoryBase, IMapper mapper)
        {
            _repositoryBase = repositoryBase;
            _mapper = mapper;
        }

        public void AddTemplate(TemplateDTO template)
        {
            try
            {
                _repositoryBase._Context.usp_InsertTemplateData(template.Name, template.Body, template.Type, template.IsSms, template.IsEmail, template.IsWhatsapp, template.IsFacebook, template.IsSlack, template.IsInstagram, template.IsViber, template.IsLine, template.IsWeChat, template.IsPushNotification);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListOfTemplates GetTemplate(string title, string type, int pageSize, int pageNo)
        {
            try
            {

                int? total = 0;
                var result = _repositoryBase._Context.usp_GetTemplateDataResultAsync(title, type, pageSize, pageNo, ref total);
                List<usp_GetTemplateDataResult> data = result;
                List<TemplateDTO> TemplatesData = _mapper.Map<List<usp_GetTemplateDataResult>, List<TemplateDTO>>(data);

                ListOfTemplates templateList = new ListOfTemplates();
                templateList.TemplateList = TemplatesData;
                templateList.Total = total ?? 0;

                return templateList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveTemplate(int id)
        {
            try
            {
                _repositoryBase._Context.usp_RemoveTemplateData(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateTemplate(TemplateDTO template)
        {
            try
            {
                _repositoryBase._Context.usp_UpdateTemplateData(template.Id, template.Name, template.Body, template.Type, template.IsSms, template.IsEmail, template.IsWhatsapp, template.IsFacebook, template.IsSlack, template.IsInstagram, template.IsViber, template.IsLine, template.IsWeChat, template.IsPushNotification);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CheckTemplateExist(int id)
        {
            try
            {
                return _repositoryBase.Get<Template>(x => x.Id == id).Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool CheckTemplateExistByName(string name)
        {
            try
            {
                return _repositoryBase.Get<Template>(x => x.Name == name).Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
