﻿using EchoChat.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EchoChat.Repository
{
    public interface IRepositoryBase : IDisposable
    {
        SPToCoreContext _Context { get; set; }
        bool Add<T>(T entity) where T : class;

        bool Update<T>(T entity) where T : class;

        bool Remove<T>(T entity) where T : class;

        IQueryable<T> Get<T>(Expression<Func<T, bool>> condition) where T : class;

        IQueryable<T> GetWith<T>(string includeEntities, Expression<Func<T, bool>> condition) where T : class;

        IQueryable<T> Get<T>() where T : class;

        bool SaveChanges();

        void AddLog(string logType, string moduleType, int moduleID, string details, Guid createdBy, bool saveChanges = false);

        int ExecuteSqlCommand(string sql);



    }
}
