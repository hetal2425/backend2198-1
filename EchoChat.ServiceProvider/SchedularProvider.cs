﻿using EchoChat.Models;
using EchoChat.Repository;
using EchoChatApp.Helper;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace EchoChat.ServiceProvider
{
    public class SchedularProvider : ISchedularProvider
    {
        ISchedularRepository schedularRepository;
        private IAuthProvider appProvider;

        public SchedularProvider(ISchedularRepository oSchedularRepository, IAuthProvider oAuthProvider)
        {
            schedularRepository = oSchedularRepository;
            appProvider = oAuthProvider;
        }
        public void AddSchedular(SchedularDTO schedular)
        {
            try
            {
                schedularRepository.AddSchedular(schedular);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GenerateJwtTokenForSchedular(LoginRequest oUser, string timeZoneOffset)
        {
            UserData user = new UserData();
            //oUser.Password = Encryption.Encrypt(oUser.Password);
            user = appProvider.CheckUserLogin(oUser);
            if (user != null && user.MobileNo != null && !string.IsNullOrEmpty(user.MobileNo))
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes("RWNob0NoYXRBcHA6QW1pdFNlbmd1cHRh");
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim(ClaimTypes.PrimarySid, user.UserID.ToString()),
                    new Claim(ClaimTypes.PrimaryGroupSid, user.ClientID.ToString()),
                    new Claim(ClaimTypes.Email, user.ClientEmail),
                    new Claim(ClaimTypes.Name, user.UserName.Trim()),
                    new Claim("TimeZoneOffset", timeZoneOffset),
                    new Claim("ClientName", user.ClientName),
                    new Claim(ClaimTypes.Role, user.IsAdmin.HasValue && user.IsAdmin.Value ? "Admin" : "User")
                    }),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                return tokenHandler.WriteToken(token);
            }
            else
            {
                return string.Empty;
            }
        }

        public List<SchedularDTO> GetSchedular()
        {
            try
            {
                return schedularRepository.GetSchedular();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListOfSchedular GetSchedulePagedData(DateTime? startDate, DateTime? endDate, int pageSize, int pageNo)
        {
            try
            {
                return schedularRepository.GetSchedulePagedData(startDate, endDate, pageSize, pageNo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveSchedule(string schedularIds)
        {
            try
            {
                schedularRepository.RemoveSchedule(schedularIds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
