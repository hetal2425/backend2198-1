﻿using EchoChat.DB;
using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EchoChat.ServiceProvider
{
    public interface IUserProvider
    {
        public void AddUserConnection(string connectionID, Guid userID);
        public string GetUserConnection(Guid userID);
        public void RemoveUserConnection(string connectionID);
        public int SubmitUsers(ExcelUsers list, Guid userID, int clientID);
        public bool CreateUpdateUser(CreateUser user, int clientID, Guid createdBy);
        public bool DeleteUser(Guid userID);
        public CreateUser GetUserByUsername(string username);
        public bool FirstTimeUpdatePassword(UpdatePassword update);
        public bool UpdatePassword(UpdatePassword update);
        bool SetGroupAdmin(GroupAdminMapping mapping);
        List<UserSmallModel> GetUsersByListIDs(List<int> listIDs);
        UserProfile GetUserByID(Guid userID);
        UserProfile GetUserBySlackID(string slackID);
        UserClaimsModel GetUserClaimsByID(Guid userID);
        bool UpdateIntroHappened(Guid userID);
        public bool UpdateUserPersonalInfo(UserPersonalInfoModel user, Guid userID);
        string GetUserPasswordByID(Guid userID);
        public bool UpdateUserPassword(UserPasswordModel user, Guid userID);
        bool SaveUpdateSetting(AppSettingModel appSetting);
        bool DeleteSetting(int appSettingId);
        List<AppSettingModel> GetAppSettingByChannel(String channel);
        List<string> GetSettingKeys(bool isKarix);
        CreateUser CreateIntegrationUser(CreateUser user, int clientID, Guid createdBy);
        List<UserListDTO> GetUserIdsFromGroup(int groupId);
    }
}
