﻿using EchoChat.Models;
using EchoChat.Repository;
using System;

namespace EchoChat.ServiceProvider
{
    public class CommonProvider : ICommonProvider
    {
        ICommonRepository commonRepository;
        public CommonProvider(ICommonRepository oCommonRepository)
        {
            commonRepository = oCommonRepository;
        }
    }
}
