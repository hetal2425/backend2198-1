﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class ReceiveEmail
    {
        public int ReceiveEmailId { get; set; }
        public string EventType { get; set; }
        public string MsgId { get; set; }
        public string ReferenceMsgId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public DateTime? Timestamp { get; set; }
        public string Content { get; set; }
    }
}
