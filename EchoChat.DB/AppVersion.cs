﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class AppVersion
    {
        public int Id { get; set; }
        public string Platform { get; set; }
        public string Version { get; set; }
        public DateTime? Date { get; set; }
    }
}
