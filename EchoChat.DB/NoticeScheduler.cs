﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class NoticeScheduler
    {
        public int NoticeSchedulerId { get; set; }
        public int NoticeId { get; set; }
        public DateTime? Date { get; set; }
        public TimeSpan? Time { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? IsRecursive { get; set; }
        public string Days { get; set; }
        public bool? IsExecuted { get; set; }
    }
}
