﻿using EchoChat.Helper;
using EchoChat.Models;
using EchoChat.ServiceProvider;
using EchoChatApp.General;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace EchoChatApp.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class DashboardController : BaseController
    {
        private INoticeProvider noticeProvider;
        private IUserProvider userProvider;
        private IReportProvider reportProvider;
        private ILog logger;
        public DashboardController(INoticeProvider oNoticeProvider, IUserProvider oUserProvider, IReportProvider oReportProvider)
        {
            noticeProvider = oNoticeProvider;
            userProvider = oUserProvider;
            reportProvider = oReportProvider;
            logger = Logger.GetLogger(typeof(DashboardController));
        }


        [HttpGet("GetGroupReportHeading")]
        public IActionResult GetGroupReportHeading()
        {
            try
            {
                GroupReportHeading result = reportProvider.GetGroupReportHeading(ClientID);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetGroupReportHeading: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetGroupReportDetails")]
        public IActionResult GetGroupReportDetails(int pageSize = 50, int pageNo = 1, string freeText = null, DateTime? fromDate = null, DateTime? toDate = null)
        {
            try
            {
                GroupReportDetailsModel result = reportProvider.GetGroupReportDetails(ClientID, freeText, fromDate, toDate, pageSize, pageNo, TimeZoneOffset);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetGroupReportDetails: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetCampaignReportHeading")]
        public IActionResult GetCampaignReportHeading(int groupID)
        {
            try
            {
                List<CampaignReportHeading> result = reportProvider.GetCampaignReportHeading(groupID);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetCampaignReportHeading: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetCampaignReportDetails")]
        public IActionResult GetCampaignReportDetails(int groupID, int pageSize = 50, int pageNo = 1, string freeText = null, DateTime? fromDate = null, DateTime? toDate = null)
        {
            try
            {
                CampaignReportDetailsModel result = reportProvider.GetCampaignReportDetails(groupID, freeText, fromDate, toDate, pageSize, pageNo, TimeZoneOffset);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetCampaignReportDetails: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetReadReportHeading")]
        public IActionResult GetReadReportHeading(int noticeID)
        {
            try
            {
                NoticeDetailDTO result = noticeProvider.GetNoticeDetailsByNoticeID(noticeID);
                UserProfile user = userProvider.GetUserByID(result.CreatedBy);

                ReadReportHeading readReportHeading = new ReadReportHeading();
                readReportHeading.NoticeTitle = result.NoticeTitle;
                readReportHeading.NoticeDate = result.NoticeDate.AddMinutes(TimeZoneOffset).ToString("dd MMM yyyy hh:mm tt");
                readReportHeading.Owner = user.FirstName;

                return Ok(readReportHeading);
            }
            catch (Exception ex)
            {
                logger.Error("GetReadReportHeading: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetReadReportDetails")]
        public IActionResult GetReadReportDetails(int noticeID, int pageSize = 50, int pageNo = 1, string freeText = null)
        {
            try
            {
                ReadReportDetailsModel result = reportProvider.GetReadReportDetails(noticeID, freeText, pageSize, pageNo, TimeZoneOffset);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetReadReportDetails: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetChannelVerification")]
        public IActionResult GetChannelVerification(int pageSize = 50, int pageNo = 1, string freeText = null)
        {
            try
            {
                ChannelVerificationModel result = reportProvider.GetChannelVerification(ClientID, pageSize, pageNo, freeText);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetChannelVerification: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("BlockNotice")]
        public IActionResult BlockNotice(int NoticeID)
        {
            try
            {
                reportProvider.BlockNotice(NoticeID);
                return Ok(new { Message = "SUCCESS" });
            }
            catch (Exception ex)
            {
                logger.Error("BlockNotice: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetChannelSavingReportDetails")]
        public IActionResult GetChannelSavingReportDetails(DateTime? fromDate = null, DateTime? toDate = null)
        {
            try
            {
                List<ChannelSavingModel> result = reportProvider.GetChannelSavingReportDetails(ClientID, fromDate, toDate, TimeZoneOffset);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetChannelSavingReportDetails: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetChannelInsightReportDetails")]
        public IActionResult GetChannelInsightReportDetails(int groupID, int noticeID, DateTime? fromDate = null, DateTime? toDate = null, int timeSlot = 0)
        {
            try
            {
                List<ChannelInsightModel> result = reportProvider.GetChannelInsightReportDetails(ClientID, groupID, noticeID, fromDate, toDate, timeSlot, TimeZoneOffset);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetChannelInsightReportDetails: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetWeekdaysEngagementReportDetails")]
        public IActionResult GetWeekdaysEngagementReportDetails(DateTime? fromDate = null, DateTime? toDate = null, int dayType = 0, int timeSlot = 0)
        {
            try
            {
                List<WeekdaysEngagementModel> result = reportProvider.GetWeekdaysEngagementReportDetails(ClientID, fromDate, toDate, dayType, timeSlot, TimeZoneOffset);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetWeekdaysEngagementReportDetails: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetCampaignSummaryReportDetails")]
        public IActionResult GetCampaignSummaryReportDetails(int groupID, int noticeID, DateTime? fromDate = null, DateTime? toDate = null)
        {
            try
            {
                List<CampaignSummaryModel> result = reportProvider.GetCampaignSummaryReportDetails(ClientID, groupID, noticeID, fromDate, toDate, TimeZoneOffset);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetCampaignSummaryReportDetails: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetChannelProgressReportDetails")]
        public IActionResult GetChannelProgressReportDetails()
        {
            try
            {
                List<ChannelProgressModel> result = reportProvider.GetChannelProgressReportDetails(ClientID, TimeZoneOffset);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetChannelProgressReportDetails: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetReadReportDetailsExcel")]
        public IActionResult GetReadReportDetailsExcel(int noticeID, int pageSize = 50, int pageNo = 1, string freeText = null)
        {
            try
            {
                ReadReportDetailsModel result = reportProvider.GetReadReportDetails(noticeID, freeText, pageSize, pageNo, TimeZoneOffset);
                var stream = ExcelProcessing.CreateExcel(result);
                string excelName = $"ReadReport_" + noticeID + ".xlsx";
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
            }
            catch (Exception ex)
            {
                logger.Error("GetReadReportDetailsExcel: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpPost("AddUpdateChannelPrediction")]
        public IActionResult AddUpdateChannelPrediction(ChannelPredictionModel channelPrediction)
        {
            try
            {
                reportProvider.AddUpdateChannelPrediction(channelPrediction);
                return Ok(new { });
            }
            catch (Exception ex)
            {
                logger.Error("AddUpdateChannelPrediction: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

    }
}