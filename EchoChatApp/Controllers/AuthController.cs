﻿using EchoChat.Channel;
using EchoChat.Helper;
using EchoChat.Models;
using EchoChat.ServiceProvider;
using EchoChatApp.General;
using EchoChatApp.Helper;
using log4net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace EchoChatApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private IAuthProvider appProvider;
        private readonly AppSettingsFromFile appSetting;
        private ILog logger;
        public AuthController(IAuthProvider oAuthProvider, IOptions<AppSettingsFromFile> oAppSettings)
        {
            appProvider = oAuthProvider;
            appSetting = oAppSettings.Value;
            logger = Logger.GetLogger(typeof(AuthController));
        }

        [Route("Login")]
        [HttpPost]
        public IActionResult Login(LoginRequest oUser)
        {
            UserData user = new UserData();
            try
            {
                oUser.Password = Encryption.Encrypt(oUser.Password);
                user = appProvider.CheckUserLogin(oUser);

                if (user != null && user.MobileNo != null && !string.IsNullOrEmpty(user.MobileNo))
                {
                    Request.Headers.TryGetValue("X-Timezone-Offset", out var timeZoneOffset);

                    logger.Info("Timezone-Offset : " + timeZoneOffset);
                    user.ClientLogo = AWSHelper.GetPresignedFileURL_Logo(user.ClientLogo);
                    timeZoneOffset = string.IsNullOrEmpty(timeZoneOffset) ? "330" : timeZoneOffset; // Hardcoded 330 if timezoneoffset is null
                    var token = GenerateJwtToken(user, timeZoneOffset.ToString());
                    user.Status = "Valid";
                    user.Token = token;
                    user.ChatUrl = appSetting.BaseAPI + "ChatHub";
                    return Ok(new { user });
                }
                else
                {
                    return BadRequest(new { Message = "Invalid User name or Password" });
                }
            }
            catch (Exception ex)
            {
                logger.Error("Login: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("GetToken")]
        [HttpPost]
        public IActionResult GetToken(LoginRequest oUser)
        {
            UserData user = new UserData();
            try
            {
                oUser.Password = Encryption.Encrypt(oUser.Password);
                user = appProvider.CheckUserLogin(oUser);

                if (user != null && user.MobileNo != null && !string.IsNullOrEmpty(user.MobileNo))
                {
                    Request.Headers.TryGetValue("X-Timezone-Offset", out var timeZoneOffset);
                    logger.Info("Timezone-Offset : " + timeZoneOffset);
                    timeZoneOffset = string.IsNullOrEmpty(timeZoneOffset) ? "330" : timeZoneOffset; // Hardcoded 330 if timezoneoffset is null
                    var token = GenerateJwtToken(user, timeZoneOffset.ToString());


                    APIAccessData accessData = new APIAccessData();
                    accessData.Token = token;
                    accessData.UserID = user.UserID;
                    accessData.UserName = user.UserName;
                    accessData.MobileNo = user.MobileNo;
                    accessData.EmailID = user.EmailID;

                    return Ok(new { accessData });
                }
                else
                {
                    return BadRequest(new { Message = "Invalid User name or Password" });
                }
            }
            catch (Exception ex)
            {
                logger.Error("GetToken: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        private string GenerateJwtToken(UserData user, string timeZoneOffset)
        {
            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSetting.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.PrimarySid, user.UserID.ToString()),
                    new Claim(ClaimTypes.PrimaryGroupSid, user.ClientID.ToString()),
                    new Claim(ClaimTypes.Email, user.ClientEmail),
                    new Claim(ClaimTypes.Name, user.UserName.Trim()),
                    new Claim("TimeZoneOffset", timeZoneOffset),
                    new Claim("ClientName", user.ClientName),
                    new Claim(ClaimTypes.Role, user.IsAdmin.HasValue && user.IsAdmin.Value ? "Admin" : "User")
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}