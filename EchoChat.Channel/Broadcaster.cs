﻿using EchoChat.Channel.Factory;
using EchoChat.Models;
using log4net;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EchoChat.Channel
{
    public class Broadcaster : IBroadcaster
    {
        private ILog logger = LogManager.GetLogger(typeof(Broadcaster));
        public ConcurrentBag<ChannelOutput> Process(MessageDetails messageDetails, List<string> channels, List<UserChannelData> userChannelDatas, List<ChannelLevelSettings> settings)
        {
            ConcurrentBag<ChannelOutput> output = new ConcurrentBag<ChannelOutput>();
            try
            {
                Task[] listOfTasks = new Task[channels.Count];
                Essentials.settings = new ConcurrentBag<ChannelLevelSettings>(settings);
                Essentials.messageDetails = messageDetails;
                Essentials.userChannelDatas = new ConcurrentBag<UserChannelData>(userChannelDatas);
                List<BaseChannel> channelsObj = new List<BaseChannel>();

                foreach (string channel in channels)
                {
                    try
                    {
                        channelsObj.Add(ChannelFactory.GetChannel(channel));
                        logger.Info($"{channel} object is created");
                    }
                    catch(Exception ex)
                    {
                        logger.Error($"{channel} object is not created {ex.Message} -- {ex.InnerException}");
                    }
                }

                int i = 0;
                foreach (BaseChannel channelObj in channelsObj)
                {
                    Task.Delay(100);
                    if (channelObj != null)
                    {
                        listOfTasks[i] = Task.Run(channelObj.Process);
                    }
                    i++;
                }

                Task.WaitAll(listOfTasks);

                channelsObj.Clear();
                output = Essentials.output;
                Essentials.output = new ConcurrentBag<ChannelOutput>();
                return output;
            }
            catch (Exception ex)
            {
                logger.Error("Process: ", ex);
                output = Essentials.output;
                Essentials.output = new ConcurrentBag<ChannelOutput>();
                return output;
            }
        }
    }
}