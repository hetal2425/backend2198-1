﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using EchoChat.Channel.Factory;
using EchoChat.ChannelModels;
using EchoChat.Constants;
using EchoChat.Helper;
using EchoChat.Models;
using log4net;
using Newtonsoft.Json;
using RestSharp;

namespace EchoChat.Channel
{
    public class WhatsappKarixChannel : BaseChannel
    {
        private static readonly log4net.ILog logger = LogManager.GetLogger(typeof(WhatsappKarixChannel));
        static string ParameterCount = string.Empty;
        public override Task Process()
        {
            try
            {
                List<ChannelLevelSettings> appSettings = Essentials.settings.Where(a => a.Key.Equals(SettingMaster.WhatsAppAPIKarix)).ToList();
                if (appSettings != null && appSettings.Count > 0)
                {
                    List<WhatsAPPKarixMessage> whatsAPPMessages = BuildMultipleWAMessage();
                    if (whatsAPPMessages == null || whatsAPPMessages.Count == 0)
                    {
                        logger.Error("Failed to send WhatsAPP Messages");
                        return Task.Delay(0);
                    }

                    logger.Info("appSettings count" + appSettings.Count);
                    int cnt = 0;

                    foreach (ChannelLevelSettings setting in appSettings)
                    {
                        int quota = int.Parse(setting.Value1) / whatsAPPMessages.Count;
                        if (quota == 0)
                        {
                            continue;
                        }

                        logger.Info(string.Format("{0} : {1}", setting.Value, quota));

                        List<UserChannelData> userList = Essentials.userChannelDatas.Skip(cnt).Take(Essentials.userChannelDatas.Skip(cnt).Count() > quota ? quota : Essentials.userChannelDatas.Skip(cnt).Count()).ToList();
                        logger.Info(string.Format("UserChannelData List count : {0} {1}", userList.Count, setting.Value));
                        foreach (var ucd in userList)
                        {
                            logger.Info(string.Format("{0} : {1}", cnt, ucd.MobileNo));

                            whatsAPPMessages = BuildMultipleWAMessage();
                            foreach (WhatsAPPKarixMessage wam in whatsAPPMessages)
                            {
                                WhatsAPPKarixMessage dummyWAM = new WhatsAPPKarixMessage();
                                dummyWAM = wam;
                                dummyWAM.message.recipient.to = ucd.CustomDetails_1;

                                if (ParameterCount == "1")
                                {
                                    if (dummyWAM.message.content.template.parameterValues._0 != null)
                                    {
                                        dummyWAM.message.content.template.parameterValues._0 = dummyWAM.message.content.template.parameterValues._0.Replace(@"{UserName}", ucd.DisplayName);
                                    }
                                    if (dummyWAM.message.content.mediaTemplate.bodyParameterValues._0 != null)
                                    {
                                        dummyWAM.message.content.mediaTemplate.bodyParameterValues._0 = dummyWAM.message.content.mediaTemplate.bodyParameterValues._0.Replace(@"{UserName}", ucd.DisplayName);
                                    }
                                }
                                else
                                {
                                    if (dummyWAM.message.content.template.parameterValues._1 != null)
                                    {
                                        dummyWAM.message.content.template.parameterValues._1 = dummyWAM.message.content.template.parameterValues._1.Replace(@"{UserName}", ucd.DisplayName);
                                        dummyWAM.message.content.mediaTemplate.bodyParameterValues._1 = dummyWAM.message.content.mediaTemplate.bodyParameterValues._1.Replace(@"{UserName}", ucd.DisplayName);
                                    }
                                }

                                //DelayTesting(ChannelMaster.WhatsappChannelKarix, ucd.UserID);
                                Send(setting.Value2, setting.Value, ucd, wam);
                            }
                            cnt++;
                        }
                    }
                }
                return Task.Delay(0);
            }
            catch (Exception ex)
            {
                logger.Error($"WA Process: {ex}");
                throw ex;
            }
        }

        private void Send(string token, string waAPI, UserChannelData ucd, WhatsAPPKarixMessage waMessage)
        {
            try
            {
                var client = new RestClient(waAPI);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authentication", "Bearer " + token);
                request.AddHeader("Content-Type", "application/json");
                string content = JsonConvert.SerializeObject(waMessage);
                content = content.Replace("_0", "0");
                content = content.Replace("_1", "1");
                if (ParameterCount == "0")
                {
                    content = content.Replace("\"0\":null", "");
                    content = content.Replace(",\"1\":null", "");
                }
                else if (ParameterCount == "1")
                {
                    content = content.Replace(",\"1\":null", "");
                }
                request.AddParameter("application/json", content, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var apiResponse = response.Content;
                    WAResponseKarix waResponse = JsonConvert.DeserializeObject<WAResponseKarix>(apiResponse);
                    logger.Info("Whatsapp Send Message API Response  ---\r\n" + apiResponse);

                    if (waResponse != null)
                    {
                        logger.Info($" MobileNo: {ucd.MobileNo} Message Id: {waResponse.mid}");
                        Essentials.PushInOutput(ChannelMaster.WhatsappKarixChannel, ucd.UserID, waResponse.mid, string.Empty);
                        return;
                    }
                }
                Essentials.PushInOutput(ChannelMaster.WhatsappKarixChannel, ucd.UserID, string.Empty, "Unknown Error", false);
            }
            catch (Exception ex)
            {
                Essentials.PushInOutput(ChannelMaster.WhatsappKarixChannel, ucd.UserID, string.Empty, ex.Message, false);
                logger.Error($"WA Send: {ex}");
                throw ex;
            }
        }

        private WhatsAPPKarixMessage BuildWAMessage(string fileName, string fileType, bool isMainMsg = true)
        {
            ChannelLevelSettings appSettings = Essentials.settings.Where(a => a.Key.Equals(SettingMaster.WhatsAppAPIKarix)).FirstOrDefault();
            string filePath = AWSHelper.GetPresignedFileURL(fileName);

            TemplateModelKarix tm = new TemplateModelKarix();
            WhatsAPPKarixMessage waMsg = new WhatsAPPKarixMessage();
            TemplateKarix template = new TemplateKarix();
            ParameterValues parameterValues = new ParameterValues();
            MediaTemplate mediaTemplate = new MediaTemplate();
            Media media = new Media();
            BodyParameterValues bodyParameterValues = new BodyParameterValues();

            TextMessage message = new TextMessage();
            message.channel = "WABA";

            Content content = new Content();
            content.preview_url = false;
            content.shorten_url = false;

            Recipient recipient = new Recipient();
            recipient.to = string.Empty;
            recipient.recipient_type = appSettings.Value3;

            Sender sender = new Sender();
            sender.from = appSettings.Details;

            Preferences preferences = new Preferences();
            preferences.webHookDNId = appSettings.Value4;

            MetaData metaData = new MetaData();
            metaData.version = "v1.0.9";
            if (fileType == EchoFileType.Image)
            {
                tm = GetTemplate("image");
                content.type = tm.ContentType;
                mediaTemplate.templateId = tm.TemplateId;
                media.type = tm.MediaType;
                media.url = filePath;
            }
            else if (fileType == EchoFileType.Video)
            {
                tm = GetTemplate("Video");
                content.type = tm.ContentType;
                mediaTemplate.templateId = tm.TemplateId;
                media.type = tm.MediaType;
                media.url = filePath;
            }
            else if (fileType == EchoFileType.Document)
            {
                tm = GetTemplate("document");
                content.type = tm.ContentType;
                mediaTemplate.templateId = tm.TemplateId;
                media.type = tm.MediaType;
                media.url = filePath;
                media.fileName = fileName;
            }
            else
            {
                tm = GetTemplate("Msg");
                content.type = tm.ContentType;
                template.templateId = tm.TemplateId;
            }
            if (isMainMsg)
            {
                if (!tm.IsWithoutParams)
                {
                    if (tm.IsGeneric)
                    {
                        if (ParameterCount == "0")
                        {

                        }
                        else if (ParameterCount == "1")
                        {
                            parameterValues._0 = Essentials.messageDetails.Message;
                            bodyParameterValues._0 = Essentials.messageDetails.Message;
                        }
                        else
                        {
                            parameterValues._0 = Essentials.messageDetails.SenderName;
                            parameterValues._1 = Essentials.messageDetails.Message;

                            bodyParameterValues._0 = Essentials.messageDetails.SenderName;
                            bodyParameterValues._1 = Essentials.messageDetails.Message;
                        }
                    }
                    else
                    {
                        parameterValues._1 = "{UserName}";
                        bodyParameterValues._1 = "{UserName}";
                    }
                }
            }

            //message.content = content;
            mediaTemplate.media = media;
            mediaTemplate.bodyParameterValues = bodyParameterValues;
            content.mediaTemplate = mediaTemplate;
            template.parameterValues = parameterValues;
            content.template = template;
            message.content = content;
            message.recipient = recipient;
            message.sender = sender;
            message.preferences = preferences;
            waMsg.message = message;
            waMsg.metaData = metaData;

            return waMsg;

        }

        private List<WhatsAPPKarixMessage> BuildMultipleWAMessage()
        {
            List<WhatsAPPKarixMessage> waMsgList = new List<WhatsAPPKarixMessage>();
            int msgCnt = 0;
            if (Essentials.messageDetails.Files != null && Essentials.messageDetails.Files.Count > 1)
            {
                foreach (EchoFile ef in Essentials.messageDetails.Files)
                {
                    WhatsAPPKarixMessage waMsg;
                    if (msgCnt == 0)
                    {
                        waMsg = BuildWAMessage(ef.FileName, ef.FileType);
                    }
                    else
                    {
                        waMsg = BuildWAMessage(string.Empty, string.Empty, false);
                    }

                    waMsgList.Add(waMsg);
                    msgCnt++;
                }
            }
            else if (Essentials.messageDetails.Files != null && Essentials.messageDetails.Files.Count > 0 && Essentials.messageDetails.Files[0] != null)
            {
                WhatsAPPKarixMessage waMsg = BuildWAMessage(Essentials.messageDetails.Files[0].FileName, Essentials.messageDetails.Files[0].FileType);
                waMsgList.Add(waMsg);
            }
            else
            {
                WhatsAPPKarixMessage waMsg = BuildWAMessage(string.Empty, string.Empty);
                waMsgList.Add(waMsg);
            }

            return waMsgList;
        }



        private TemplateModelKarix GetTemplate(string type)
        {
            TemplateModelKarix templateModel = new TemplateModelKarix();

            List<ChannelLevelSettings> appSettings = Essentials.settings.Where(a => a.Key.Equals(SettingMaster.WhatsAppTemplateKarix) && a.Value1.ToLower() == type.ToLower()).ToList();

            foreach (ChannelLevelSettings cls in appSettings)
            {
                if (!string.IsNullOrEmpty(Essentials.messageDetails.TemplateID))
                    if (!string.IsNullOrEmpty(cls.Value) && Essentials.messageDetails.TemplateID.Contains(cls.Value))
                    {
                        templateModel.TemplateId = cls.Value5;
                        templateModel.ContentType = cls.Value6;
                        templateModel.MediaType = cls.Value1.ToLower();
                        templateModel.IsGeneric = true;
                        templateModel.IsWithoutParams = false;
                        ParameterCount = cls.Value7;
                        break;
                    }
            }

            if (string.IsNullOrEmpty(templateModel.TemplateId))
            {
                ChannelLevelSettings setting = appSettings.Where(a => a.Value1.ToLower() == type.ToLower() && (a.Value3 == null || a.Value3 == string.Empty)).FirstOrDefault();
                templateModel.TemplateId = setting.Value5;
                templateModel.ContentType = setting.Value6;
                templateModel.MediaType = setting.Value1.ToLower();
                templateModel.IsGeneric = true;
                templateModel.IsWithoutParams = false;
            }

            return templateModel;
        }
    }
}
