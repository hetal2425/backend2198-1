﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using EchoChat.Channel.Factory;
using EchoChat.Constants;
using EchoChat.Models;
using log4net;
using Newtonsoft.Json;

namespace EchoChat.Channel
{
    public class PushNotificationChannel : BaseChannel
    {
        private static readonly log4net.ILog logger = LogManager.GetLogger(typeof(PushNotificationChannel));

        private static string _androidAppKey { get; set; }
        protected static string androidAppKey
        {
            get
            {
                if (string.IsNullOrEmpty(_androidAppKey))
                {
                    _androidAppKey = Essentials.settings.Where(s => s.Key.Equals(Constants.SettingMaster.PushNotificationAndroidKey)).Select(s => s.Value).FirstOrDefault();
                }
                return _androidAppKey;
            }
        }

        private static string _androidAppUrl { get; set; }
        protected static string androidAppUrl
        {
            get
            {
                if (string.IsNullOrEmpty(_androidAppUrl))
                {
                    _androidAppUrl = Essentials.settings.Where(s => s.Key.Equals(Constants.SettingMaster.PushNotificationAndroidURL)).Select(s => s.Value).FirstOrDefault();
                }
                return _androidAppUrl;
            }
        }
        public override Task Process()
        {
            try
            {
                var task = Task.Run(() =>
            {
                Parallel.ForEach(Essentials.userChannelDatas, (ucd) =>
                {
                    //DelayTesting(ChannelMaster.PushNotificationChannel, ucd.UserID);
                    Send(ucd);
                });
            });

                task.Wait();

                return Task.Delay(0);
            }
            catch (Exception ex)
            {
                logger.Error($"Process: {ex}");
                throw ex;
            }
        }

        public void Send(UserChannelData ucd)
        {
            try
            {
                if (string.IsNullOrEmpty(ucd.CustomDetails_2))
                {
                    return;
                }
                WebRequest tRequest = WebRequest.Create(_androidAppUrl);
                tRequest.Method = "post";
                tRequest.Headers.Add(string.Format("Authorization: key={0}", _androidAppKey));
                tRequest.ContentType = "application/json";
                var payload = new
                {
                    to = ucd.CustomDetails_2,
                    data = new
                    {
                        title = Essentials.messageDetails.Title,
                        message = Essentials.messageDetails.Message,
                        notice = Essentials.messageDetails.CustomDetails_3
                    },
                    priority = "high"
                };

                string postbody = JsonConvert.SerializeObject(payload).ToString();
                Byte[] byteArray = Encoding.UTF8.GetBytes(postbody);
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            if (dataStreamResponse != null) using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                {
                                    String sResponseFromServer = tReader.ReadToEnd();
                                    //result = sResponseFromServer;
                                    Essentials.PushInOutput(ChannelMaster.PushNotificationChannel, ucd.UserID, string.Empty, string.Empty);
                                }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Essentials.PushInOutput(ChannelMaster.PushNotificationChannel, ucd.UserID, string.Empty, ex.Message, false);
                logger.Error($"Send: {ex}");
                throw ex;
            }
        }
    }
}
