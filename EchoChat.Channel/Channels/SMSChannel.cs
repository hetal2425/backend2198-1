﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using EchoChat.Channel.Factory;
using EchoChat.Constants;
using EchoChat.Models;
using log4net;

namespace EchoChat.Channel
{
    public class SMSChannel : BaseChannel
    {
        private ILog logger = LogManager.GetLogger(typeof(SMSChannel));

        public override Task Process()
        {
            try
            {
                string msg = Essentials.messageDetails.Message;

                List<ChannelLevelSettings> appSettings = Essentials.settings.Where(a => a.Key.Equals(SettingMaster.SMSSettings)).ToList();

                string apiURL = string.Empty;

                foreach(ChannelLevelSettings cls in appSettings)
                {
                    if(msg.Contains(cls.Value1))
                    {
                        apiURL = cls.Value;
                        break;
                    }
                }

                if (!string.IsNullOrEmpty(apiURL))
                {
                    apiURL = apiURL.Replace(@"{message}", System.Web.HttpUtility.UrlPathEncode(msg));

                    foreach (var ucd in Essentials.userChannelDatas)
                    {
                        //DelayTesting(ChannelMaster.SMSChannel, ucd.UserID);
                        Send(ucd.UserID, ucd.UserName, ucd.MobileNo, apiURL);
                    }
                }
                return Task.Delay(0);
            }
            catch (Exception ex)
            {
                logger.Error("Process: " + ex.Message);
                throw ex;
            }
        }

        public void Send(string userID, string userName, string mobileNo, string apiURL)
        {
            using var httpClient = new HttpClient();
            try
            {
                apiURL = apiURL.Replace(@"{UserName}", userName);
                apiURL = apiURL.Replace(@"{destination}", mobileNo);

                var res = httpClient.PostAsync(apiURL, null);
                res.Wait();
                if (res.Result.StatusCode == HttpStatusCode.OK)
                {
                    var stringData = res.Result.Content.ReadAsStringAsync();
                    stringData.Wait();
                    string[] SMSMsgID = stringData.Result.Split(':');
                    if (SMSMsgID != null && SMSMsgID.Count() > 1 && SMSMsgID[1] != null)
                    {
                        Essentials.PushInOutput(ChannelMaster.SMSChannel, userID, SMSMsgID[1], string.Empty);
                        logger.Info($"The sms with message Id {SMSMsgID[1]} sent successfully to {mobileNo} on {DateTime.UtcNow}");
                        return;
                    }

                    Essentials.PushInOutput(ChannelMaster.SMSChannel, userID, string.Empty, stringData.Result, false);
                }
            }
            catch (Exception ex)
            {
                Essentials.PushInOutput(ChannelMaster.SMSChannel, userID, string.Empty, ex.Message, false);
                logger.Error("Sendsms: " + ex.Message);
                throw ex;
            }
        }
    }
}
