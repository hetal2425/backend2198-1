﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class NoticeDTO
    {
        public string NoticeTitle { get; set; }
        public string NoticeDetail { get; set; }
        public string NoticeDetailHTML { get; set; }
        public string NoticeDate { get; set; }
        public string ClientID { get; set; }
        public string GroupID { get; set; }
        public string IsReply { get; set; }
        public string IsSms { get; set; }
        public string IsSlack { get; set; }
        public string ParentID { get; set; }
        public string FileType { get; set; }
        public string FileName { get; set; }
        public IFormFile StreamWithData { get; set; }
        public string IsEmail { get; set; }
        public string IsWhatsapp { get; set; }
        public string DeviceID { get; set; }
        public string IsFacebook { get; set; }
        public string IsViber { get; set; }
        public string IsTelegram { get; set; }
        public string Createdby { get; set; } // need to remove this later after auth
        public string BroadcastToAllChannel { get; set; }

        public DateTime? Date { get; set; }
        public TimeSpan? Time { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? IsRecursive { get; set; }
        public string Days { get; set; }
        public bool? IsScheduled { get; set; }
        public bool IsAlreadyExists { get; set; }
        public string TemplateID { get; set; }
    }
}
